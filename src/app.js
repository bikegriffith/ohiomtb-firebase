import React from "react";
import { Router, Route, IndexRoute } from 'react-router'
import history from './history';
import NoMatch from "./components/404";
import AppShell from "./components/app-shell";
import AllTrails from "./components/all-trails";
import MyTrails from "./components/my-trails";
import RecentUpdates from "./components/recent-updates";


import './css/main.scss';

let injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();


React.render((
  <Router history={history}>
    <Route path="/" component={AppShell}>
      <IndexRoute component={AllTrails}/>
      <Route path="recent-updates" component={RecentUpdates} />
      <Route path="my-trails" component={MyTrails} />
      <Route path="*" component={NoMatch}/>
    </Route>
  </Router>
), document.body);
