import Firebase from 'firebase';


class DataStore {
  constructor() {
    this.firebase = new Firebase("https://blazing-heat-5265.firebaseio.com/");

    this.trails = [];
    this.trailStore = this.firebase.child('trails');
    this.trailStore.limitToLast(10).on('child_added', this.onChildAdded.bind(this, 'trails'));

    this.messages = [];
    this.messageStore = this.firebase.child('messages');
    this.messageStore.limitToLast(10).on('child_added', this.onChildAdded.bind(this, 'messages'));
  }

  onChildAdded(type, snapshot) {
    this[type].push({
      key: snapshot.key(), val: snapshot.val()
    });
  }


}

let dataStore = new DataStore();

export default dataStore;

