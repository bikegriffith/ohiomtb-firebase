import React from 'react';
import dataStore from '../data-store'
import TrailCard from './trail-card';


export default class AllTrails extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      trails: dataStore.trails,
      auth: dataStore.firebase.getAuth()
    };
  }

  componentWillMount() {
    console.warn('Subscribing to firebase events');
    this.setState({
      trails: dataStore.trails
    });
    //this.trailStore = firebase.child('trails');
    //this.trailStore.on('child_added', this.onMessage.bind(this));
  }

  componentWillUnmount() {
    console.warn('Unloading firebase events');
    //this.trailStore.off('child_added', this.onMessage.bind(this));
  }

  render() {
    return (
      <div>
        <ul>
          {this.state.trails.map(t => <li key={t.key}><TrailCard trail={t.val} /></li>)}
        </ul>
      </div>
    )
  }

  /*onMessage(snapshot) {
    this.setState({
      trails: this.state.trails.concat([{
          key: snapshot.key(), val: snapshot.val()
      }])
    });
  }*/

}
