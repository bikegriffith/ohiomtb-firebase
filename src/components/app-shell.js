import React from "react";
import { Router, Route, Link } from 'react-router'
import history from '../history'

var mui          = require('material-ui'),
    AppBar       = mui.AppBar,
    Paper        = mui.Paper,
    IconButton   = mui.IconButton,
    Tabs         = mui.Tabs,
    Tab          = mui.Tab,
    FontIcon     = mui.FontIcon;

//require('../../../node_modules/material-design-icons/sprites/css-sprite/sprite-navigation-white.css');

export default class AppShell extends React.Component {
  render() {
    var logo = (
        <img style={{height:'60px', marginTop:'-5px'}}
             src="http://www.arval-electronics.co.uk/css/tennis-league/common/WEATHER-SKETCHED/outline-256x256/wso-heavy-hail-showers.png"
             />
    );
    return (
      <div>
        <Paper className='topNav' rounded={false}>
          <AppBar iconElementLeft={logo} title='Ohio MTB' zDepth={0}>
            <Tabs>
              <Tab label="ALL TRAILS" style={{height:'64px'}} onClick={this.go.bind(this, '/')} />
              <Tab label="NEWEST UPDATES" style={{height:'64px'}} onClick={this.go.bind(this, '/recent-updates')} />
              <Tab label="MY TRAILS" style={{height:'64px'}} onClick={this.go.bind(this, '/my-trails')} />
            </Tabs>
          </AppBar>
        </Paper>
        {this.props.children}
      </div>
    )
  }

  go(url) {
    history.replaceState(null, url);
  }
}

