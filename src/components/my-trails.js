import React from 'react';
import dataStore from '../data-store'
import Login from './login';

export default class MyTrails extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
          messages: []
        };
    }

    refreshData() {
      this.setState({messages: dataStore.messages});
    }

    componentWillMount() {
      this.refreshData()
    }

    componentWillUnmount() {
    }

    render() {
      return (
        <div>
          <ul>
            {this.state.messages.map(m => this.renderItem(m))}
          </ul>
          <input ref="message" type="text" />
          <button onClick={this.ping.bind(this)}>Ping</button>
          <Login />
        </div>
      )
    }

    renderItem(message) {
      return ( <li key={message.key}>{message.val.text}</li> );
    }

    ping() {
      let message = this.refs.message.getDOMNode();
      dataStore.messageStore.push({name:'mike', text:message.value});
      message.value = '';
      this.refreshData();
    }

}
