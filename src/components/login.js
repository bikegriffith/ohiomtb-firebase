import React from 'react';
import dataStore from '../data-store'

var mui          = require('material-ui'),
    RaisedButton = mui.RaisedButton;


export default class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
          auth: dataStore.firebase.getAuth()
        };
    }

    render() {
      if (this.state.auth) {
        return (
            <p>
              Logged in as {this.state.auth[this.state.auth.provider].username}
              <RaisedButton onClick={this.logout.bind(this)} label='Log out' />
            </p>
        );
      } else {
        return (
            <ul>
              <li><RaisedButton onClick={this.loginViaOauth.bind(this, 'twitter')} label='Login via Twitter' /></li>
              <li><RaisedButton onClick={this.loginViaOauth.bind(this, 'facebook')} label='Login via Facebook' /></li>
              <li><RaisedButton onClick={this.loginViaOauth.bind(this, 'ggogle')} label='Login via Google' /></li>
            </ul>
        );
      }
    }

    logout() {
      dataStore.firebase.unauth();
      this.setState({auth: null});
    }

    loginViaOauth(provider) {
      dataStore.firebase.authWithOAuthPopup(provider, this.onAuthComplete.bind(this));
    }

    onAuthComplete(error, authData) {
      if (error) {
        console.error('onAuthComplete error', error);
      } else {
        console.log('onAuthComplete success', authData);
        this.setState({
          auth: authData
        });
      }
    }
}
