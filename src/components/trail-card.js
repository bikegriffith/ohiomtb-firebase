import React from 'react';

export default class TrailCard extends React.Component {

  render() {
    let trail = this.props.trail;
    return (
      <div style={{height:'60px'}}>
        <strong>{trail.name}</strong>
        <p>Status: {trail.status}</p>
      </div>
    );
  }

}
